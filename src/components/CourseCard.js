import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default function CourseCard({course}){
    
    // Destructuring the props
    const {name, description, price, _id} = course

    // Using the state
    const [count, setCount] = useState(0) 
    const [seats, setSeats] = useState(15)
    const [isOpen, setIsOpen] = useState(true)
    /* 
    function enroll (){
        slots()
        if(seats > 0){
            setCount(count + 1)
        }
    }

    function slots(){
        if(seats > 0){
            setSeats(seats - 1)
        }
        if(seats < 1){
            return alert('No more slots available')
        }
    }

    // Effect in React is just like side effects/effects in real life where everytime something happens within the component, a function or condition runs.
    // You may also listen or watch a specific state for changes instead of watching/listening to the whole component
    useEffect(()=> {
        if(seats === 0){
            setIsOpen(false)
        }
    }, [seats])
 */
    return(
        <Card className = "course my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>PHP{price}</Card.Text>
                <Link className='btn btn-primary' to={`/courses/${_id}`}>Details</Link>                       
            </Card.Body>
        </Card>
    )
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}