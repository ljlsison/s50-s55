import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext, useEffect} from 'react'

export default function Logout(){
    const {unsetUser, setUser} = useContext(UserContext)

    // Using the context, clear the contents of the localStorage
    unsetUser()

    // An effect which removes the user mail from the global user state that comes from context
    useEffect(()=> {
        setUser({
            id:null
        })
    },[])

    alert('You have successfully logged out!')


return(
    <Navigate to="/login"/>
)
}