import { useEffect,useState } from "react";
import CourseCard from "../components/CourseCard";
import Loading from "../components/Loading";
// import courses_data from "../data/courses"; No needed anymore

export default function Courses(){
    const [courses,setCourses] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        // Sets the loading state to true
        setLoading(true)
        fetch(`${process.env.REACT_APP_API_URL}/courses`)
        .then(response => response.json())
        .then(result => {
            setCourses(
                result.map(course => {
                    return (
                        <CourseCard key={course._id} course={course}/>
                    )
                })
            )

            setLoading(false)
        })
    },[])

    return(
            (loading)?
            <Loading/>
            :
        <>
            {courses}
        </>
    ) 
}